const mongoose = require('mongoose')
const { Schema } = mongoose;

const chatSchema = new Schema({
  topic:    {type:String,required:true},
  user:     {type:Schema.Types.ObjectId,ref:'User'},
  message:  {type:String,required:true},
  created:  {type:Date,required:true},
  replies:  [{
    by:{type:Schema.Types.ObjectId,ref:'User'},
    message:{type:String},
    replied_at:{type:Date},
  }],
})
const Chat = mongoose.model('Chat',chatSchema)
module.exports = Chat
