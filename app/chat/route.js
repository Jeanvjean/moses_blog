const express = require('express');
const router = require('express-promise-router')();


const chatController = require('./chatController');

router.route('/send_message')
  .post(chatController.chat)

router.route('/reply')
  .post(chatController.reply)

router.route('/chat')
  .get(chatController.to_chat)

router.route('/active_chat/:id')
  .get(chatController.all)

  module.exports = router;
