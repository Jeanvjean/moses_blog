const Chat = require('./chat')

module.exports = {
  chat:async(req,res)=>{
    var {message,topic} = req.body
    var user = req.user._id
    var date = Date.now()
    var chat = new Chat({
      topic,
      user,
      message,
      created:date
    })
    chat.save()
    res.status(200).json({
      message:'message sent',
      chat:chat
    })
  },
  reply:async(req,res)=>{
      var {chat_id,message} = req.body

      // console.log(chat_id,reply)
      var chat = await Chat.findById(chat_id)
      var reply = ({
        by:req.user._id,
        message:message,
        replied_at:Date.now()
      })
      chat.replies.push(reply)

      var new_chat = await chat.save()
      res.status(200).json({chat:new_chat,message:"sent"})
  },
  to_chat:async(req,res)=>{
    var chat = await Chat.find({})
    res.render('home/chat',{chat:chat})
  },
  all:async(req,res)=>{
    var chat_id = req.params.id
    console.log(chat_id)
    var chat =await Chat.findById(chat_id)
    console.log(chat)
    res.render('home/active_chat',{chat})
  }
}
